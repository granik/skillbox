import json

from flask import Flask, request, render_template
import datetime

app = Flask(__name__)


def get_time_now():
    return datetime.datetime.now().strftime("%d-%m-%Y %H:%M")


db_file = "./data/db.json"  # Путь к файлу
json_db = open(db_file, "rb")
data = json.load(json_db)
messages_list = data["messages_list"]


def save_messages():
    data = {
        "messages_list": messages_list,
    }
    json_db = open(db_file, "w")
    json.dump(data,json_db)


# Функция, которая умеет выводить одно сообщение
def print_message(message):
    print(f"[{message['sender']}]: {message['text']} / {message['date']}")
    print("-" * 50)


# Функция добавления нового сообщения
def add_message(name, txt):
    message = {
        "text": txt,
        "sender": name,
        "date": get_time_now(),
        # Хочется, чтобы текущая дата подставлялась автоматически
    }
    messages_list.append(message)  # Добавляем новое сообщение в список


@app.route('/')
def index_page():
    return "Hello, welcome to the Skillbox chat!"


@app.route('/get_messages')
def get_messages():
    return {"messages": messages_list}


@app.route('/send_message')
def send_message():
    name = request.args["name"]
    text = request.args["text"]
    if 3 <= len(name) <= 100 and 1 <= len(text) <= 1000:
        add_message(name, text)
        save_messages()
        return "OK"
    else:
        return "ERROR"


@app.route('/form')
def form():
    return render_template("form.html")


app.run()
